import {render as lives} from './lives';
import timer from './timer';

export const render = (inGame = true) => `
  <header class="header">
    <div class="header__back">
      <span class="back">
        <img src="img/arrow_left.svg" width="45" height="45" alt="Back">
        <img src="img/logo_small.png" width="101" height="44">
      </span>
    </div>
    ${inGame ? lives() + timer : ``}
  </header>
`;
