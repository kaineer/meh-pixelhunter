import {get} from '../utils/state';

const items = (stats) => {
  let total = get(`questions.total`);
  let result = [];

  const li = (name) => {
    return `<li class="stats__result stats__result--${name}"></li>`;
  };

  for (let i = 0; i < total; i++) {
    result.push(
        li(stats[i] || `unknown`)
    );
  }

  return result.join(``);
};

export const render = (game) => {
  let gameItems;

  if (typeof (game) === `undefined`) {
    gameItems = items(get(`stats`));
  } else {
    //
  }

  return `<div class="stats"><ul class="stats">${gameItems}</ul></div>`;
};
