import {get} from '../utils/state';

const asset = (type, index) => {
  return get(`assets.${type}s.${index}`);
};

const rect = ({width, height}, {width: rectWidth, height: rectHeight}) => {
  let resWidth;
  let resHeight;
  const cWidth = rectWidth / width;
  const cHeight = rectHeight / height;

  if (cWidth > cHeight) {
    // Ужимаем по высоте
    resWidth = width * cHeight;
    resHeight = height * cHeight;
  } else {
    // Ужимаем по ширине
    resWidth = width * cWidth;
    resHeight = height * cWidth;
  }

  return {
    w: resWidth,
    h: resHeight
  };
};

export const render = ({type, index}, {width, height, alt = `Не указан`}) => {
  const obj = asset(type, index);
  const {w, h} = rect(obj, {width, height});

  return `<img src="${obj.url}" alt="${alt}" width="${w}" height="${h}">`;
};
