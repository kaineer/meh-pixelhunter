import {get} from '../utils/state';

const emptyHeart = `<img src="img/heart__empty.svg" class="game__heart" alt="Life" width="32" height="32">`;

const fullHeart = `<img src="img/heart__full.svg" class="game__heart" alt="Life" width="32" height="32">`;

const hearts = (lives) => {
  return [3, 2, 1].
    map((index) => (index <= lives) ? fullHeart : emptyHeart).
    join(``);
};

export const render = () => {
  return `<div class="game__lives">${hearts(get(`lives`))}</div>`;
};
