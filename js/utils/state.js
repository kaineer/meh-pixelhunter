const photos = [
  {
    url: `http://i.imgur.com/1KegWPz.jpg`,
    width: 1080,
    height: 720
  },

  {
    url: `https://i.imgur.com/DiHM5Zb.jpg`,
    width: 1264,
    height: 1864
  },

  {
    url: `http://i.imgur.com/DKR1HtB.jpg`,
    width: 1120,
    height: 2965
  }
];

const paintings = [
  {
    url: `https://k42.kn3.net/CF42609C8.jpg`,
    width: 600,
    height: 831
  },

  {
    url: `https://k42.kn3.net/D2F0370D6.jpg`,
    width: 468,
    height: 354
  },

  {
    url: `https://k32.kn3.net/5C7060EC5.jpg`,
    width: 1200,
    height: 900
  }
];

const initialState = {
  lives: 2,
  stats: [ // results up until now
    `wrong`,
    `slow`,
    `fast`,
    `correct`
  ],
  questions: {
    total: 10,
    current: -1
  },
  games: [
    // To be removed in real program
    {
      type: `two_pictures`,
      pictures: [
        {
          type: `photo`,
          index: 0
        },
        {
          type: `painting`,
          index: 1
        }
      ]
    },

    {
      type: `one_picture`,
      pictures: [
        {
          type: `photo`,
          index: 1
        }
      ]
    },

    {
      type: `one_of_three`,
      pictures: [
        {
          type: `photo`,
          index: 2
        },

        {
          selected: true,
          type: `painting`,
          index: 1
        },

        {
          type: `photo`,
          index: 0
        }
      ]
    }
  ],
  assets: {
    photos,
    paintings
  }
};

const deepClone = (obj) => {
  let result = Array.isArray(obj) ? [] : {};

  Object.keys(obj).forEach((key) => {
    const value = obj[key];

    if (value && typeof (value) === `object`) {
      result[key] = deepClone(value);
    } else {
      result[key] = value;
    }
  });

  return result;
};

let state = deepClone(initialState);

export const get = (path) => {
  if (typeof (path) !== `string`) {
    return state;
  }

  const parts = path.split(`.`);

  let value = state;

  for (let i = 0; i < parts.length && value; i++) {
    value = value[parts[i]];
  }

  return value;
};
