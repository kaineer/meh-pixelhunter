//

import {get} from './state';
import {expect} from 'chai';

describe(`state`, () => {
  it(`should be a function`, () => {
    expect(get).to.be.a(`function`);
  });
});
