export default (markup) => {
  const div = document.createElement(`div`);

  if (typeof (markup) === `function`) {
    markup = markup();
  }

  if (typeof (markup) === `function`) {
    div.innerHTML = markup();
  } else {
    div.innerHTML = markup;
  }

  return {
    remove() {
      while (this.$el.firstChild) {
        this.$el.removeChild(this.$el.firstChild);
      }
    },
    appendTo(element) {
      this.$el = element;

      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }

      while (div.firstChild) {
        element.appendChild(div.firstChild);
      }
    },
    find(selector) {
      return this.$el.querySelector(selector);
    },
    findAll(selector) {
      return this.$el.querySelectorAll(selector);
    }
  };
};
