import render from './render';

export default (root, template, obj = {}) => {
  return Object.assign({
    show() {
      this.$el = render(template);
      this.$el.appendTo(root);

      this._run(`attachListeners`);
    },
    hide() {
      if (this.$el) {
        if (typeof (this.detachListeners) === `function`) {
          this.detachListeners();
        } else {
          this.off();
        }

        this.$el.remove();
      }
    },
    on(eventType, selector, cb) {
      let el = selector;

      if (typeof (selector) === `string`) {
        el = this.$el.find(selector);
      }

      if (el) {
        el.addEventListener(eventType, cb);

        this._pushHandler(el, eventType, cb);
      }
    },
    off() {
      (this._pushHandlers || []).forEach(({el, type, callback}) => {
        el.removeEventListener(type, callback);
      });
    },
    switchTo(component) {
      this.hide();
      component.show();
    },
    _run(method, ...args) {
      if (typeof (this[method]) === `function`) {
        this[method](...args);
      }
    },
    _pushHandler(el, type, callback) {
      if (!this._pushHandlers) {
        this._pushHandlers = [];
      }

      this._pushHandlers.push({el, type, callback});
    }
  }, obj);
};
