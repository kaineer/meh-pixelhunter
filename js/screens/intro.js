import footer from '../partials/footer';
import component from '../utils/component';
import greeting from './greeting';

const template = `
  <div id="main" class="central__content">
    <div id="intro" class="intro">
      <h1 class="intro__asterisk">*</h1>
      <p class="intro__motto"><sup>*</sup> Это не фото. Это рисунок маслом нидерландского художника-фотореалиста Tjalf Sparnaay.</p>
    </div>
  </div>
  ${footer}
`;

const root = document.querySelector(`main.central`);

export default component(root, template, {
  attachListeners() {
    this.on(`click`, `.intro__asterisk`, () => {
      this.switchTo(greeting);
    });
  }
});
