import footer from '../partials/footer';
import {render as header} from '../partials/header';
import component from '../utils/component';

import greeting from './greeting';
import game1 from './game-1';

const template = `
  ${header(false)}
  <div class="rules">
    <h1 class="rules__title">Правила</h1>
    <p class="rules__description">Угадай 10 раз для каждого изображения фото <img
      src="img/photo_icon.png" width="16" height="16"> или рисунок <img
      src="img/paint_icon.png" width="16" height="16" alt="">.<br>
      Фотографиями или рисунками могут быть оба изображения.<br>
      На каждую попытку отводится 30 секунд.<br>
      Ошибиться можно не более 3 раз.<br>
      <br>
      Готовы?
    </p>
    <form class="rules__form">
      <input class="rules__input" type="text" placeholder="Ваше Имя">
      <button class="rules__button  continue" type="submit" disabled>Go!</button>
    </form>
  </div>
  ${footer}
`;

const root = document.querySelector(`main.central`);

export default component(root, template, {
  attachListeners() {
    const input = this.$el.find(`.rules__input`);
    const button = this.$el.find(`.rules__button`);
    const back = this.$el.find(`.header__back`);

    this.on(`keyup`, input, () => {
      button.disabled = (input.value === ``);
    });

    this.on(`click`, button, (event) => {
      event.preventDefault();
      this.switchTo(game1);
    });

    this.on(`click`, back, (event) => {
      event.preventDefault();
      this.switchTo(greeting);
    });
  }
});
