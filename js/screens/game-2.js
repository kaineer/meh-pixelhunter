import footer from '../partials/footer';
import {render as header} from '../partials/header';
import {render as results} from '../partials/stats';
import {render as image} from '../partials/image';

import component from '../utils/component';

import greeting from './greeting';
import game3 from './game-3';

import {get} from '../utils/state';

const data = get(`games.1`);

const appendix = (alt) => {
  return {
    width: 705,
    height: 455,
    alt
  };
};

const renderOption = (picture) => {
  const alt = `Option 1`;
  const question = `question1`;

  return `
    <div class="game__option">
      ${image(picture, appendix(alt))}
      <label class="game__answer game__answer--photo">
        <input name="${question}" type="radio" value="photo">
        <span>Фото</span>
      </label>
      <label class="game__answer game__answer--paint">
        <input name="${question}" type="radio" value="paint">
        <span>Рисунок</span>
      </label>
    </div>
  `;
};

const render = () => `
  ${header()}
  <div class="game">
    <p class="game__task">Угадай, фото или рисунок?</p>
    <form class="game__content  game__content--wide">
      ${renderOption(data.pictures[0])}
    </form>
    ${results()}
  </div>
  ${footer}
`;

const root = document.querySelector(`main.central`);

export default component(root, render, {
  attachListeners() {
    const radioButtons = this.$el.findAll(`.game__answer input`);

    radioButtons.forEach((button) => {
      this.on(`click`, button, () => {
        this.switchTo(game3);
      });
    });

    this.on(`click`, `.header__back`, () => {
      this.switchTo(greeting);
    });
  }
});
