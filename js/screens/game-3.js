import footer from '../partials/footer';
import {render as header} from '../partials/header';
import {render as results} from '../partials/stats';
import {render as image} from '../partials/image';

import component from '../utils/component';

import greeting from './greeting';
import stats from './stats';

import {get} from '../utils/state';

const data = get(`games.2`);

const appendix = (alt) => {
  return {
    width: 304,
    height: 455,
    alt
  };
};

const renderOption = (picture, idx) => {
  const selected = picture.selected;

  return `
    <div class="game__option${selected ? ` game__option--selected` : ``}">
      ${image(picture, appendix(`Option 1`))}
    </div>
  `;
};

const renderPictures = (pictures) => {
  return pictures.map(renderOption).join(``);
};

const render = () => `
  ${header()}
  <div class="game">
    <p class="game__task">Найдите рисунок среди изображений</p>
    <form class="game__content game__content--triple">
      ${renderPictures(data.pictures)}
    </form>
    ${results()}
  </div>
  ${footer}
`;

const root = document.querySelector(`main.central`);

export default component(root, render, {
  attachListeners() {
    const options = this.$el.findAll(`.game__option`);

    options.forEach((option) => {
      this.on(`click`, option, () => {
        this.switchTo(stats);
      });
    });

    this.on(`click`, `.header__back`, () => {
      this.switchTo(greeting);
    });
  }
});
