import footer from '../partials/footer';
import {render as header} from '../partials/header';
import {render as results} from '../partials/stats';
import {render as image} from '../partials/image';

import component from '../utils/component';

import greeting from './greeting';
import game2 from './game-2';

import {get} from '../utils/state';

const data = get(`games.0`);

const appendix = (alt) => {
  return {
    width: 468,
    height: 458,
    alt
  };
};

const renderOption = (picture, idx) => {
  const alt = idx === 0 ? `Option 1` : `Option 2`;
  const question = idx === 0 ? `question1` : `question2`;

  return `
    <div class="game__option">
      ${image(picture, appendix(alt))}
      <label class="game__answer game__answer--photo">
        <input name="${question}" type="radio" value="photo">
        <span>Фото</span>
      </label>
      <label class="game__answer game__answer--paint">
        <input name="${question}" type="radio" value="paint">
        <span>Рисунок</span>
      </label>
    </div>
  `;
};

const renderPictures = (pictures) => {
  return pictures.map(renderOption).join(``);
};

const render = () => `
  ${header()}
  <div class="game">
    <p class="game__task">Угадайте для каждого изображения фото или рисунок?</p>
    <form class="game__content">
      ${renderPictures(data.pictures)}
    </form>
    ${results()}
  </div>
  ${footer}
`;

const root = document.querySelector(`main.central`);

export default component(root, render, {
  attachListeners() {
    const radioButtons = this.$el.findAll(`.game__answer input`);

    const form = this.$el.find(`.game__content`);
    const question1 = form.elements.question1;
    const question2 = form.elements.question2;

    radioButtons.forEach((el) => {
      this.on(`click`, el, (event) => {
        if (question1.value && question2.value) {
          this.switchTo(game2);
        }
      });
    });

    this.on(`click`, `.header__back`, () => {
      this.switchTo(greeting);
    });
  },
});
